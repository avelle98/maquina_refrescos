library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity Maquina_refrescos_tb is
end;

architecture bench of Maquina_refrescos_tb is

  component Maquina_refrescos
    Port ( 
    clk:in std_logic;
    dinerok:in std_logic_vector(1 downto 0);
    current_money: in std_logic_vector(7 downto 0);
    --bebidok: in std_logic;
    --seleccion_beb:in std_logic_vector(7 downto 0);
    reset:in std_logic;
    cocacola,fanta,agua:in std_logic;
    salida: out std_logic_vector(7 downto 0)
    );
  end component;

  signal clk: std_logic;
  signal dinerok: std_logic_vector(1 downto 0);
  signal current_money: std_logic_vector(7 downto 0);
  --signal bebidok: std_logic;
  --signal seleccion_beb: std_logic_vector(7 downto 0);
  signal reset: std_logic;
  signal agua:std_logic;
  signal fanta:std_logic;
  signal cocacola:std_logic;
  signal salida: std_logic_vector(7 downto 0) ;


  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: Maquina_refrescos port map ( clk           => clk,
                                    dinerok       => dinerok,
                                    current_money => current_money,
                                    --bebidok       => bebidok,
                                    --seleccion_beb => seleccion_beb,
                                    reset         => reset,
                                    agua          => agua,
                                    fanta         => fanta,
                                    cocacola      => cocacola,
                                    salida        => salida );

  stimulus: process
  begin
  
    -- Put initialisation code here
    reset<='1';
    dinerok<="00";
    current_money<="00000000";
    --bebidok<='0';
    --seleccion_beb<="11001101";
    agua<='0';
    cocacola<='0';
    fanta<='0';
    wait for 1000 ns;
    
    reset<='0';
    wait for 1000 ns;
    
    current_money<="01011010";--90
    wait for 1000 ns;
    
    current_money<="01100100";--100
    dinerok<="01";
    wait for 1000 ns;
    
   --seleccion_beb<="11001001";--201
   agua<='1';
   --bebidok<='1';
   wait for 500 ns;
   
   --seleccion_beb<="11001010";--202
   --bebidok<='1';
   agua<='0';
   cocacola<='1';--me da igual si pulsootra bebida
   wait for 500 ns;
   
   reset<='1';
   cocacola<='0';
   --bebidok<='0';
   dinerok<="00";
   current_money<="00000000";
   wait for 1000 ns;
   
   reset<='0';
   current_money<="00001010";--10
   wait for 1000 ns;
   
   current_money<="01101110";--110
   dinerok<="10";--se�al de error
   wait for 1000 ns;
    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;
