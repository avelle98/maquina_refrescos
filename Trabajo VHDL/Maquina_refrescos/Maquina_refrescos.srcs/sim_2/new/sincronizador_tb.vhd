library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity sincronizador_tb is
end;

architecture bench of sincronizador_tb is

  component sincronizador
   Port (
   sync_in: IN STD_LOGIC;
   clk: IN STD_LOGIC;
   sync_out: OUT STD_LOGIC
   );
  end component;

  signal sync_in: STD_LOGIC;
  signal clk: STD_LOGIC;
  signal sync_out: STD_LOGIC ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: sincronizador port map ( sync_in  => sync_in,
                                clk      => clk,
                                sync_out => sync_out );

  stimulus: process
  begin
  
    -- Put initialisation code here
    sync_in<='0';
    wait for 10 ms;
    sync_in<='1';
    wait for 10 ms;

    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;
