library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity selector_bebida_tb is
end;

architecture bench of selector_bebida_tb is

  component selector_bebida
    Port (
      cocacola,fanta,agua:in std_logic;
      seleccion:out std_logic_vector(7 downto 0);
      bebidok: out std_logic
     );
  end component;

  signal cocacola,fanta,agua: std_logic;
  signal seleccion: std_logic_vector(7 downto 0);
  signal bebidok: std_logic ;

begin

  uut: selector_bebida port map ( cocacola  => cocacola,
                                  fanta     => fanta,
                                  agua      => agua,
                                  seleccion => seleccion,
                                  bebidok   => bebidok );

  stimulus: process
  begin
  
    -- Put initialisation code here
    wait for 1000ns;
    cocacola<='1';
    wait for 1000ns;
  

    -- Put test bench stimulus code here

    wait;
  end process;


end;
