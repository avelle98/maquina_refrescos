library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity BCD_to_display_tb is
end;

architecture bench of BCD_to_display_tb is

  component BCD_to_display
      PORT (
          entrada: IN  STD_LOGIC_VECTOR(5 downto 0);
          salida : OUT STD_LOGIC_VECTOR(7 downto 0)
      );
  end component;

  signal entrada: STD_LOGIC_VECTOR(5 downto 0);
  signal salida: STD_LOGIC_VECTOR(7 downto 0) ;

begin

  uut: BCD_to_display port map ( entrada => entrada,
                                 salida  => salida );

  stimulus: process
  begin
  
    -- Put initialisation code here
    entrada <= "000000";
    wait for 1000 ns;
    entrada <= "000100";
    wait for 1000 ns;
    entrada <= "001010";
     wait for 1000 ns;

    -- Put test bench stimulus code here

    wait;
  end process;


end;
