library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity money_counter_tb is
end;

architecture bench of money_counter_tb is

  component money_counter
    Port (
    c10,c20,c50,euro1:in std_logic;
    reset_money_counter:in std_logic;
    current_money:out std_logic_vector(7 downto 0);
    dinerok:out std_logic_vector(1 downto 0)
     );
  end component;
  
  signal c10,c20,c50,euro1: std_logic;
  signal reset_money_counter: std_logic;
  signal current_money: std_logic_vector(7 downto 0);
  signal dinerok: std_logic_vector(1 downto 0) ;

begin

  uut: money_counter port map ( c10                 => c10,
                                c20                 => c20,
                                c50                 => c50,
                                euro1               => euro1,
                                reset_money_counter => reset_money_counter,
                                current_money       => current_money,
                                dinerok             => dinerok );

  stimulus: process
  begin
  
    -- Put initialisation code here
    c10<='0';
    c20<='0';
    c50<='0';
    euro1<='0';
    reset_money_counter<='0';
    wait for 1000 ns;
    
    c20<='1';
    wait for 1000 ns;
    
    c10<='1';
    c20<='0';
    wait for 1000 ns;
    
    c20<='1';
    c10<='0';
    wait for 1000 ns; 
    
    reset_money_counter<='1';
    c20<='0';
    wait for 1000 ns; 
    
    euro1<='1';
    reset_money_counter<='0';
    wait for 1000 ns;
    
    -- Put test bench stimulus code here

    wait;
  end process;


end;
  
