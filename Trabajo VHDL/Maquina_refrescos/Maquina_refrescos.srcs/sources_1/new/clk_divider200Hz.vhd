library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_divider_200hz is
    Generic (frec: integer:=124999);  -- default value is for 200hz
    Port ( 
        clk:in std_logic;
        reset:in std_logic;
        clk_out:out std_logic
    );
end clk_divider_200hz;

architecture Behavioral of clk_divider_200hz is
signal clk_sig: std_logic;
begin
process (clk,reset)
variable cnt:integer:=0;
begin
	if (reset='1') then
        cnt:=0;
		clk_sig<='0';
    elsif clk'event and clk='1' then
		if (cnt=frec) then
		  cnt:=0;
		  clk_sig<=not(clk_sig);
		else
		  cnt:=cnt+1;
	    end if;
	end if;
end process;
clk_out<=clk_sig;
end Behavioral;
