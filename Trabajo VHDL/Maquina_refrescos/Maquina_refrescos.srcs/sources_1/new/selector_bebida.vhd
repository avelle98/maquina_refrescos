
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity selector_bebida is
  Port (
    cocacola,fanta,agua:in std_logic;
    seleccion:out std_logic_vector(7 downto 0);
    bebidok: out std_logic
   );
end selector_bebida;

architecture Behavioral of selector_bebida is

begin
process(cocacola,fanta,agua)
begin
    if(cocacola='1')then
        seleccion<="11001001";--201 en binario
    elsif(fanta='1')then
        seleccion<="11001010";--202 en binario
    elsif(agua='1')then
        seleccion<="11001011";--203 en binario
    else
        seleccion<="11001101";--205 en binario poner refresco
    end if;
end process;
   bebidok<= cocacola or fanta or agua;    

end Behavioral;