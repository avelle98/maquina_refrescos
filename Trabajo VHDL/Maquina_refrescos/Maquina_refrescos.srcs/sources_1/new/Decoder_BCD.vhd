
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;


entity Decoder_BCD is
  Port (
    entrada:in std_logic_vector(7 downto 0);
    S0,S1,S2,S3,S4,S5,S6,S7:out std_logic_vector(5 downto 0)--para pasarlo en binario al BCD to display
   );
end Decoder_BCD;

architecture Behavioral of Decoder_BCD is
signal letra0,letra1,letra2,letra3,letra4,letra5,letra6,letra7:std_logic_vector(5 downto 0);
begin
decoder_BCD:process(entrada)
variable uds:integer range 0 to 9;
variable dec:integer range 0 to 9;
variable cen:integer range 0 to 9;
variable mil:integer range 0 to 9;
variable count:integer range 0 to 400:=0;
begin
    count:=to_integer(unsigned(entrada));
    if(count<200)then
        mil:=0;
        cen:=count/100;
        dec:=(count-(cen*100))/10;
        uds:=count -(cen*100+dec*10);
        letra0<="000000";--O
        --9 mod 5 = 4
        letra1<=std_logic_vector(to_unsigned(cen,s1'length));
        letra2<=std_logic_vector(to_unsigned(dec,s2'length));
        letra3<=std_logic_vector(to_unsigned(uds,S3'length));
        letra4<="001110";--x"86";--E
        letra5<="011110";--x"E3";--U
        letra6<="011011";--x"AF";--R 
        letra7<="011000";--x"C0";--O
        
    elsif(count=201)then
        letra0<="001100";--x"C6";--C
        letra1<="011000";--x"C0";--O
        letra2<="001100";--x"C6";--C
        letra3<="001010";--x"88";--A
        letra4<="001100";--x"C6";--C
        letra5<="011000";--x"C0";--O
        letra6<="010101";--x"C7";--L
        letra7<="001010";--x"88";--A
        
    elsif(count=202)then
        letra0<="001111";--x"8E";--F
        letra1<="001010";--x"88";--A
        letra2<="010111";--x"AB";--N
        letra3<="011101";--x"87";--T
        letra4<="001010";--x"88";--A
        letra5<="111111";--x"FF";--NADA
        letra6<="111111";--x"FF";--NADA
        letra7<="111111";--x"FF";--NADA
        
    elsif(count=203)then
        letra0<="001010";--x"88";--A
        letra1<="010000";--x"90";--G
        letra2<="011110";--x"E3";--U
        letra3<="001010";--x"88";--A
        letra4<="111111";--x"FF";--NADA
        letra5<="111111";--x"FF";--NADA
        letra6<="111111";--x"FF";--NADA
        letra7<="111111";--x"FF";--NADA
        
    elsif(count=204)then
        letra0<="001110";--x"86";--E
        letra1<="011011";--x"AF";--R       
        letra2<="011011";--x"AF";--R 
        letra3<="011000";--x"C0";--O
        letra4<="011011";--x"AF";--R 
        letra5<="111111";--x"FF";--NADA
        letra6<="111111";--x"FF";--NADA
        letra7<="111111";--x"FF";--NADA
        
    elsif(count=205)then 
        letra0<="011011";--x"AF";--R 
        letra1<="001110";--x"86";--E      
        letra2<="001111";--x"8E";--F 
        letra3<="011011";--x"AF";--R
        letra4<="001110";--x"86";--E  
        letra5<="011100";--x"92";--S
        letra6<="001100";--x"C6";--C
        letra7<="011000";--x"C0";--O
           
    else
        letra0<="100101";--x"F7";--_
        letra1<="100101";--x"F7";--_    
        letra2<="100101";--x"F7";--_
        letra3<="100101";--x"F7";--_
        letra4<="100101";--x"F7";--_
        letra5<="100101";--x"F7";--_
        letra6<="100101";--x"F7";--_
        letra7<="100101";--x"F7";--_
    end if;
end process;
S0<=letra0;
S1<=letra1;
S2<=letra2;
S3<=letra3;
S4<=letra4;
S5<=letra5;
S6<=letra6;
S7<=letra7;
    

end Behavioral;
