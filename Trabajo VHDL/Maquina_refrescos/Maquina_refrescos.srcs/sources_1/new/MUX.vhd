
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MUX is
  Port (
    a:in std_logic_vector(7 downto 0);
    b:in std_logic_vector(7 downto 0);
    c:in std_logic_vector(7 downto 0);
    sel:in std_logic_vector(1 downto 0);
    salida:out std_logic_vector(7 downto 0)
   );
end MUX;

architecture Behavioral of MUX is

begin
process(sel,a,b,c)
begin
    case sel is
        when "00" => salida <= (others => '0');
        when "01" => salida <= a;--S1
        when "10" => salida <= b;--S2(se�al de error que se a�adira en la entidad top)
        when "11" => salida <= c;--S3
        when others => salida <= (others => '0');
    end case;
 end process;

end Behavioral;
