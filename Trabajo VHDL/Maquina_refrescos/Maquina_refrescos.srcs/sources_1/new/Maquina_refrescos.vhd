
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Maquina_refrescos is
  Port ( 
  clk:in std_logic;
  dinerok:in std_logic_vector(1 downto 0);
  current_money: in std_logic_vector(7 downto 0);
  --bebidok: in std_logic;--si se ha pulsado una bebida
  --seleccion_beb:in std_logic_vector(7 downto 0);
  reset:in std_logic;
  
  cocacola,fanta,agua:in std_logic;
  salida: out std_logic_vector(7 downto 0)
  );
end Maquina_refrescos;


architecture behavioral of Maquina_refrescos is
type state_type is(S1,S2,S3,S4);
signal state, next_state:state_type;
signal bebida: std_logic_vector(7 downto 0);--new

begin
sync_proc: process (clk)
begin
   if rising_edge(clk) then
    if (reset = '1') then
    state <= S1;
    else
    state <= next_state;
    end if;
   end if;
end process;

output_decode:process(state,current_money)--,seleccion_beb)
begin
    case(state)is
    when S1=> salida<=current_money;
    when S2=> salida<="11001101";--205--seleccion_beb;
    when S3=> salida<="11001100";--el numero 204 en binario que corresponde a error.
    when S4=> salida<=bebida;--seleccion_beb; --COORRREGIR!! el estado 4 es para q la salida se mantenga con el refresco seleccionado anteriormente.
    end case;
end process;
             
next_state_decode:process(state,dinerok,agua,cocacola,fanta)--, bebidok) 
begin
    next_state<=S1;
    case(state)is
    when S1=>
            if(dinerok = "01")then --"01" dinero clavado (1?); "00" no se ha pasado; "10" dinero excesivo (>1?)
                next_state<=S2;-- S2 estado de dinerok
            elsif(dinerok = "10")then
                next_state<=S3;--S3 es el estado de error           
            elsif(dinerok = "00") then
                next_state<=S1;
            end if;
            
    when S2=>
            if(cocacola='1')then
                bebida<="11001001";--201
            elsif(fanta='1')then
                bebida<="11001010";--202
            elsif(agua='1')then
                bebida<="11001011";--203
            end if;
            if(cocacola or fanta or agua)= '1' then
                next_state<=S4;
            else
                next_state<=S2;
            end if;
    when S3=>next_state<=S3;
    when S4=>next_state<=S4;
    when others=>next_state<=S1;
            
    end case;   
end process;

end behavioral;
