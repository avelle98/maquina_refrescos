library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Top is
    
  Port (
    clk:in std_logic;
    cocacola,fanta,agua:in std_logic;
    reset:in std_logic;
    c10,c20,c50,euro1:in std_logic;
    reset_money_counter:in std_logic;
    display_number : out  STD_LOGIC_VECTOR (7 downto 0);
    display_selection : out  STD_LOGIC_VECTOR (7 downto 0) 
   );
end Top;

architecture Behavioral of Top is
    signal debouncer_c10:std_logic;
    signal debouncer_c20:std_logic;
    signal debouncer_c50:std_logic;
    signal debouncer_euro1:std_logic;
    signal debouncer_reset_money_counter:std_logic;
    signal debouncer_agua:std_logic;
    signal debouncer_cocacola:std_logic;
    signal debouncer_fanta:std_logic;
    signal debouncer_reset:std_logic;
    --signal selector_maquina_seleccion:std_logic_vector(7 downto 0);
    --signal selector_maquina_bebidok:std_logic;
    signal money_counter_maquina_dinerok:std_logic_vector(1 downto 0);
    signal money_counter_maquina_current_money:std_logic_vector(7 downto 0);
    signal clk_divider_1hz_maquina_refrescos:std_logic;
    signal clk_divider_200hz_display_refresh:std_logic;
    signal maquina_refrescos_decoderBCD:std_logic_vector(7 downto 0);
    signal decoderBCD_display_refresh_S0:std_logic_vector(5 downto 0);
    signal decoderBCD_display_refresh_S1:std_logic_vector(5 downto 0);
    signal decoderBCD_display_refresh_S2:std_logic_vector(5 downto 0);
    signal decoderBCD_display_refresh_S3:std_logic_vector(5 downto 0);
    signal decoderBCD_display_refresh_S4:std_logic_vector(5 downto 0);
    signal decoderBCD_display_refresh_S5:std_logic_vector(5 downto 0);
    signal decoderBCD_display_refresh_S6:std_logic_vector(5 downto 0);
    signal decoderBCD_display_refresh_S7:std_logic_vector(5 downto 0);
    signal BCD_to_display_s0:std_logic_vector(7 downto 0);
    signal BCD_to_display_s1:std_logic_vector(7 downto 0);
    signal BCD_to_display_s2:std_logic_vector(7 downto 0);
    signal BCD_to_display_s3:std_logic_vector(7 downto 0);
    signal BCD_to_display_s4:std_logic_vector(7 downto 0);
    signal BCD_to_display_s5:std_logic_vector(7 downto 0);
    signal BCD_to_display_s6:std_logic_vector(7 downto 0);
    signal BCD_to_display_s7:std_logic_vector(7 downto 0);
    
    
component debouncer_top is
    port (
        clk	: in std_logic;
	btn_in	: in std_logic;
	btn_out	: out std_logic);
end component;
    
--component selector_bebida 
--    Port(
--    cocacola,fanta,agua:in std_logic;
--    seleccion:out std_logic_vector(7 downto 0);
--   bebidok: out std_logic
--   );
--end component;

component money_counter is
  Port (
  c10,c20,c50,euro1:in std_logic;
  reset_money_counter:in std_logic;
  current_money:out std_logic_vector(7 downto 0);--numero en binario de 0 a 200
  dinerok:out std_logic_vector(1 downto 0)
   );
end component;

component Maquina_refrescos is
  Port ( 
  clk:in std_logic;
  dinerok:in std_logic_vector(1 downto 0);
  current_money: in std_logic_vector(7 downto 0);
 -- bebidok: in std_logic;--si se ha pulsado una bebida
 -- seleccion_beb:in std_logic_vector(7 downto 0);
  reset:in std_logic;
  cocacola,fanta,agua:in std_logic;
  salida: out std_logic_vector(7 downto 0)
  );
end component;

component clk_divider_1hz is
    Generic (frec: integer:=50000000);  -- default value is for 1hz
    Port ( 
        clk:in std_logic;
        reset:in std_logic;
        clk_out:out std_logic
    );
end component;

component clk_divider_200hz is
    Generic (frec: integer:=124999);  -- default value is for 200hz
    Port ( 
        clk:in std_logic;
        reset:in std_logic;
        clk_out:out std_logic
    );
end component;

component Decoder_BCD is
  Port (
    entrada:in std_logic_vector(7 downto 0);
    S0,S1,S2,S3,S4,S5,S6,S7:out std_logic_vector(5 downto 0)--vector de 7 bits mas el bit del punto del display
   );
end component;

component BCD_to_display is
    PORT (
        entrada: IN  STD_LOGIC_VECTOR(5 downto 0);
        salida : OUT STD_LOGIC_VECTOR(7 downto 0)
    );
end component;

component display_refresh is
    Port ( clk : in  STD_LOGIC;
           s0,s1,s2,s3,s4,s5,s6,s7 : IN std_logic_vector(7 downto 0);
           display_number : out  STD_LOGIC_VECTOR (7 downto 0);
           display_selection : out  STD_LOGIC_VECTOR (7 downto 0));
end component;

begin

    inst_debouncer_c10: debouncer_top port map(
        clk=>clk,
        btn_in=>c10,
        btn_out=>debouncer_c10
    );
    
    inst_debouncer_c20: debouncer_top port map(
        clk=>clk,
        btn_in=>c20,
        btn_out=>debouncer_c20
    );
    
    inst_debouncer_c50: debouncer_top port map(
        clk=>clk,
        btn_in=>c50,
        btn_out=>debouncer_c50
    );
    
    inst_debouncer_euro1: debouncer_top port map(
        clk=>clk,
        btn_in=>euro1,
        btn_out=>debouncer_euro1
    );
    
    inst_debouncer_reset_money_counter: debouncer_top port map(
        clk=>clk,
        btn_in=>reset_money_counter,
        btn_out=>debouncer_reset_money_counter
    );
    
    inst_debouncer_agua: debouncer_top port map(
        clk=>clk,
        btn_in=>agua,
        btn_out=>debouncer_agua
    );
    
    inst_debouncer_cocacola: debouncer_top port map(
        clk=>clk,
        btn_in=>cocacola,
        btn_out=>debouncer_cocacola
    );
    
    inst_debouncer_fanta: debouncer_top port map(
        clk=>clk,
        btn_in=>fanta,
        btn_out=>debouncer_fanta
    );
    
    inst_debouncer_reset: debouncer_top port map(
        clk=>clk,
        btn_in=>reset,
        btn_out=>debouncer_reset
    );
    
--    inst_selector_bebida_maquina_refrescos: selector_bebida port map(  
--        cocacola=>debouncer_cocacola,
--        fanta=>debouncer_fanta,
--        agua=>debouncer_agua,
--        seleccion=>selector_maquina_seleccion,
--        bebidok=>selector_maquina_bebidok
--   );
    
    inst_money_counter_maquina_refrescos: money_counter port map(
        c10=>debouncer_c10,
        c20=>debouncer_c20,
        c50=>debouncer_c50,
        euro1=>debouncer_euro1,
        reset_money_counter=>debouncer_reset_money_counter,
        dinerok=>money_counter_maquina_dinerok,
        current_money=>money_counter_maquina_current_money
    );
    
    inst_clk_divider_1hz: clk_divider_1hz port map(
        clk=>clk,
        reset=>debouncer_reset,
        clk_out=>clk_divider_1hz_maquina_refrescos
    );
    
    inst_clk_divider_200hz: clk_divider_200hz port map(
        clk=>clk,
        reset=>debouncer_reset,
        clk_out=>clk_divider_200hz_display_refresh
    );
    
    inst_maquina_refrescos_decoder: maquina_refrescos port map(
        clk=>clk_divider_1hz_maquina_refrescos,
        dinerok=>money_counter_maquina_dinerok,
        current_money=>money_counter_maquina_current_money,
        --bebidok=>selector_maquina_bebidok,
        --seleccion_beb=>selector_maquina_seleccion,
        reset=>debouncer_reset,
        cocacola=>debouncer_cocacola,
        fanta=>debouncer_fanta,
        agua=>debouncer_agua,
        salida=>maquina_refrescos_decoderBCD
    );
    
    inst_decoder_display_refresh: Decoder_BCD port map(
        entrada=>maquina_refrescos_decoderBCD,
        S0=>decoderBCD_display_refresh_S0,
        S1=>decoderBCD_display_refresh_S1,
        S2=>decoderBCD_display_refresh_S2,
        S3=>decoderBCD_display_refresh_S3,
        S4=>decoderBCD_display_refresh_S4,
        S5=>decoderBCD_display_refresh_S5,
        S6=>decoderBCD_display_refresh_S6,
        S7=>decoderBCD_display_refresh_S7
    );
    
    inst_bcd_to_display_0: BCD_to_display port map(
        entrada=>decoderBCD_display_refresh_S0,
        salida=>BCD_to_display_s0
    );
    
    inst_bcd_to_display_1: BCD_to_display port map(
        entrada=>decoderBCD_display_refresh_S1,
        salida=>BCD_to_display_s1
    );
    
    inst_bcd_to_display_2: BCD_to_display port map(
        entrada=>decoderBCD_display_refresh_S2,
        salida=>BCD_to_display_s2
    );
    
    inst_bcd_to_display_3: BCD_to_display port map(
        entrada=>decoderBCD_display_refresh_S3,
        salida=>BCD_to_display_s3
    );
    
    inst_bcd_to_display_4: BCD_to_display port map(
        entrada=>decoderBCD_display_refresh_S4,
        salida=>BCD_to_display_s4
    );
    
    inst_bcd_to_display_5: BCD_to_display port map(
        entrada=>decoderBCD_display_refresh_S5,
        salida=>BCD_to_display_s5
    );
    
    inst_bcd_to_display_6: BCD_to_display port map(
        entrada=>decoderBCD_display_refresh_S6,
        salida=>BCD_to_display_s6
    );
    
    inst_bcd_to_display_7: BCD_to_display port map(
        entrada=>decoderBCD_display_refresh_S7,
        salida=>BCD_to_display_s7
    );
    
    inst_display_refresh: display_refresh port map(
        clk=>clk_divider_200hz_display_refresh,
        s0=>BCD_to_display_s0,
        s1=>BCD_to_display_s1,
        s2=>BCD_to_display_s2,
        s3=>BCD_to_display_s3,
        s4=>BCD_to_display_s4,
        s5=>BCD_to_display_s5,
        s6=>BCD_to_display_s6,
        s7=>BCD_to_display_s7,
        display_number=>display_number,
        display_selection=>display_selection
    );
end Behavioral;
