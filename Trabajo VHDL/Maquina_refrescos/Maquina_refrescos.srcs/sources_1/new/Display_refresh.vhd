library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity display_refresh is
    Port ( clk : in  STD_LOGIC;
           s0,s1,s2,s3,s4,s5,s6,s7 : IN std_logic_vector(7 downto 0);
           display_number : out  STD_LOGIC_VECTOR (7 downto 0);
           display_selection : out  STD_LOGIC_VECTOR (7 downto 0));
end display_refresh;

architecture Behavioral of display_refresh is

begin

	muestra_displays:process (clk, s0,s1,s2,s3,s4,s5,s6,s7)
	variable cnt:integer range 0 to 7;
	begin
		if (clk'event and clk='1') then 
			if cnt=7 then
				cnt:=0;
			else
				cnt:=cnt+1;
			end if;
		end if;
		
		case cnt is
				when 0 => display_selection<="01111111";
						    display_number<=s0;
				
				when 1 => display_selection<="10111111";
						    display_number<=s1;
				
				when 2 => display_selection<="11011111";
						    display_number<=s2;
				
				when 3 => display_selection<="11101111";
						    display_number<=s3;
						    
				when 4 => display_selection<="11110111";
						    display_number<=s4;	
						    
			    when 5 => display_selection<="11111011";
						    display_number<=s5;	
						    
				when 6 => display_selection<="11111101";
						    display_number<=s6;	
						    
                when 7 => display_selection<="11111110";
						    display_number<=s7;				
			end case;

	end process;

end Behavioral;
